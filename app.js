var express = require('express');
var bodyParser = require('body-parser');

var app = express();

var urlencodeParser = bodyParser.urlencoded({ extended: false });

//Set the Templating Engine
app.set('view engine', 'ejs');

//Using Middleware
app.use('/assets', express.static('assets'));

app.get('/', function(req, res) {
    //Middleware is code that is run between the 
    //request and the response.
    res.render('index');
});

app.get('/contact', function(req, res) {
    // Request object parses all the query params.
    //console.log(req.query);
    res.render('contact', {qs: req.query});
});

app.post('/contact', urlencodeParser, function(req, res) {
    console.log(req.body);
    res.render('contact-success', {data: req.body});
});

app.get('/profile/:name', function(req, res) {
    var data = {age: 29, job: 'ninja', hobbies: ['eating', 'fighting', 'fishing']};
    res.render('profile', {person: req.params.name, data: data});
});

app.listen(3000);

